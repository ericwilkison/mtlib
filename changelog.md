# Change Log

# v0.0.9 - 2019-01-23
- Update testing information in the readme, and setup.py requirements
- Fix exception handling, Catches pycoap error and raises MTException
- Fix a couple of the integration tests

# v0.0.8 - 2019-01-22
- Large change! Switch underlying lib from aiocoap to pycoap

# v0.0.4 - 2019-01-02
- Initial version using aiocoap