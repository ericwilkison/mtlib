##!/usr/bin/python3

import argparse
import os
import sys
import unittest
import logging

def main():
    """
    Test runner

    Note: method for skipping slow tests pulled from https://sateeshkumarb.wordpress.com/2011/06/16/skipping-tests-in-unittests/
    """
    print ("Test Runner")

    parser = argparse.ArgumentParser()
    parser.add_argument('test_name', nargs='?', default="test", help="The test name to run. (i.e. 'test.test_observe') Uses the same naming format as 'python3 -m unitest'.")
    parser.add_argument("-v", "--verbose", help='Enable verbose output', action="store_true")
    parser.add_argument("-f", "--failfast", help="Stop runnning tests on the first failure", action="store_true")
    parser.add_argument("--slow", help='Run slow tests', action="store_true")
    group = parser.add_argument_group('group')
    group.add_argument("-d", "--debug", help='Enable debug level logs', action="store_true")
    group.add_argument("-t", "--trace", help='Enable trace level logs', action="store_true")
    
    args = parser.parse_args()

    #Enable Logging
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    elif args.trace:
        logging.basicConfig(level=logging.TRACE)

    # Set the verbosity
    verbosity = 2 if args.verbose else 1

    # Slow tests
    if args.slow:
        global RUN_SLOW_TESTS
        RUN_SLOW_TESTS = True

    # Create the test suite
    if args.test_name == "test":
        module_path = os.path.abspath( os.path.dirname(sys.argv[0]) )
        suite = unittest.TestLoader().discover(module_path, pattern = "test_*.py")
    else:
        suite = unittest.TestLoader().loadTestsFromName(args.test_name)

    #run the tests
    unittest.TextTestRunner(verbosity=verbosity, failfast=args.failfast).run(suite)


if __name__ == "__main__":
    main()
